package com.bestvike.jenkins.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class GoodsController {
    @RequestMapping("/hello")
    public Map<String, Object> goodsTest(String param) {
        Map<String,Object> map = new HashMap<>();
        System.out.println("输入的参数为： " + param);
        map.put("param",param);
        return map;
    }
}
